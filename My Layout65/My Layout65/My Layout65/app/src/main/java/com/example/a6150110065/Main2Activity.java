package com.example.a6150110065;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;

public class Main2Activity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
    }
    public void  browser1(View view){
        Intent browserIntent=new Intent(Intent.ACTION_VIEW,Uri.parse("https://www.facebook.com/"));
        startActivity(browserIntent);
    }
    public void  browser2(View view){
        Intent browserIntent=new Intent(Intent.ACTION_VIEW,Uri.parse("https://www.instagram.com/"));
        startActivity(browserIntent);
    }
}
